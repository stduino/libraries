/***************************************************************
*Copyright(c) 2020思特诺(Stduino)All right reserved.
*
*This library is open source and free for individual users. 
*
*For commercial use, please contact server001@stduino.com.
***************************************************************/

#include "Stduino.h"

volatile uint8_t _adc_state=0;

void setmode(uint8_t pin,uint8_t mode)
{
 RCC->APB2ENR|=_gpio_rcc(pin);
 (_gpio_prt(pin)&0x08)?
 (_gpio_grp(pin)->CRH &= ~(0x0F<<((_gpio_prt(pin)&0x07)<<2))):
 (_gpio_grp(pin)->CRL &= ~(0x0F<<(_gpio_prt(pin)<<2)));
 (_gpio_prt(pin)&0x08)?
 (_gpio_grp(pin)->CRH |= mode<<((_gpio_prt(pin)&0x07)<<2)):
 (_gpio_grp(pin)->CRL |= mode<<(_gpio_prt(pin)<<2));
}

void setadc(uint8_t pin)
{
 if((_adc_state&(1<<((_adc_group[pin]&0x0F)>>2))))
 {
  return ;
 }
 RCC->APB2ENR|=_adc_rcc(pin);
 RCC->CFGR|=0xC000;
 _adc_grp(pin)->CR1&=0XF0FEFF;
 _adc_grp(pin)->CR2&=0xFFE1F7FD;
 _adc_grp(pin)->CR2|=0x001E0000;
 _adc_grp(pin)->SQR1&=0xFF0FFFFF;
 _adc_grp(pin)->SMPR2&=0xFFFFFFF8;
 _adc_grp(pin)->SMPR2|=0x07;
 _adc_grp(pin)->CR2|=0x0D;
 _adc_state|=1<<((_adc_data(pin)&0x0F)>>2);
}

void settim(uint8_t pin)
{
 (isAdvancedtimer(pin))?(RCC->APB2ENR|=_tim_rcc(pin)):(RCC->APB1ENR|=_tim_rcc(pin));
 _tim_grp(pin)->ARR=4095-1;
 _tim_grp(pin)->PSC=35-1;
 *(uint32_t *)((uint32_t)_tim_grp(pin)+0x18+(((_tim_prt(pin)-1)>>1)<<2))|=0x68<<(8*(!(_tim_prt(pin)%2)));
 _tim_grp(pin)->CCER|=1<<((_tim_prt(pin)-1)<<2);
 _tim_grp(pin)->CR1=0x81;
 
 if(isAdvancedtimer(pin))
 {
  _tim_grp(pin)->BDTR |= 0x8000;
 }
}

void freetim(uint8_t pin)
{
 _tim_grp(pin)->CCER&=~(1<<((_tim_prt(pin)-1)<<2));
}

void pinMode(uint8_t pin,uint8_t mode)
{
 switch(mode)
 {
  case OUTPUT:
   setmode(pin,OP_PP_50);
   _gpio_grp(pin)->ODR |= 0<<_gpio_prt(pin);
   break;
   
  case INPUT:
   setmode(pin,IP_FL);
   break;
   
  case INPUT_PULLUP:
   setmode(pin,IP_UP);
   _gpio_grp(pin)->ODR|=1<<_gpio_prt(pin);
   break;
   
  case INPUT_PULLDW:
   setmode(pin,IP_DW);
   _gpio_grp(pin)->ODR&=~(1<<_gpio_prt(pin));
   break;
   
  case INPUT_ANALOG:
    setmode(pin,IP_AN);
    setadc(pin);
   break;
   
  case OUTPUT_PULSE:
   setmode(pin,OP_AP_50);
   settim(pin);
   break;
   
  case INPUT_PULSE:
   setmode(pin,IP_FL);
   freetim(pin);
   break;
 }
}

uint16_t analogRead(uint8_t pin) 
{
 if(!!(_adc_data(pin)))
 {
  _adc_grp(pin)->SQR3&=0XFFFFFFE0;
  _adc_grp(pin)->SQR3|=_adc_prt(pin);
  _adc_grp(pin)->CR2|=0x00400000;
  while(!(_adc_grp(pin)->SR&0x02))
  {
   ;
  }
  return _adc_grp(pin)->DR; 
 }else
 {
  return 0;
 }
}

/***************************************************************
*Copyright(c) 2020思特诺(Stduino)All right reserved.
*
*This library is open source and free for individual users. 
*
*For commercial use, please contact server001@stduino.com.
***************************************************************/
