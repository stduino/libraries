/***************************************************************
*Copyright(c) 2020思特诺(Stduino)All right reserved.
*
*This library is open source and free for individual users. 
*
*For commercial use, please contact service001@stduino.com.
***************************************************************/

#ifndef StduinoBasic_h
#define	StduinoBasic_h	

#include "Stduino.h"
#include "StduinoTime.h"
#include "StduinoPulse.h"
#include "StduinoSerial.h"
#include "StduinoMath.h"
#include "StduinoString.h"
#include "StduinoCharacter.h"


void setup(void);
void loop(void);

#endif 

/***************************************************************
*Copyright(c) 2020思特诺(Stduino)All right reserved.
*
*This library is open source and free for individual users. 
*
*For commercial use, please contact service001@stduino.com.
***************************************************************/
