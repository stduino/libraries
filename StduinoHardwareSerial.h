/***************************************************************
*Copyright(c) 2020思特诺(Stduino)All right reserved.
*
*This library is open source and free for individual users. 
*
*For commercial use, please contact service001@stduino.com.
***************************************************************/

#ifndef StduinoHardwareSerial_h
#define	StduinoHardwareSerial_h

#ifdef __cplusplus
extern "C" {
#endif

#include "stm32f10x_usart.h"
#include "Stduino.h"

void Serial_begin(uint8_t _usart_x,uint32_t Baud);
void Serial_write(uint8_t _usart_x,char Char);
void USART1_IRQHandler(void);
void USART2_IRQHandler(void);
void USART3_IRQHandler(void);
char Serial_read(uint8_t _usart_x);
uint8_t Serial_available(uint8_t _usart_x);
char Serial_peek(uint8_t _usart_x);
char* Serial_peekString(uint8_t _usart_x);
char* Serial_readString(uint8_t _usart_x);

#ifdef __cplusplus
}
#endif

#endif

/***************************************************************
*Copyright(c) 2020思特诺(Stduino)All right reserved.
*
*This library is open source and free for individual users. 
*
*For commercial use, please contact service001@stduino.com.
***************************************************************/
