/***************************************************************
*Copyright(c) 2020思特诺(Stduino)All right reserved.
*
*This library is open source and free for individual users. 
*
*For commercial use, please contact service001@stduino.com.
***************************************************************/

#ifndef StduinoPulse_h
#define	StduinoPulse_h

#include "Stduino.h"

#define LSBFIRST 0
#define MSBFIRST 1

void analogWrite(uint8_t,uint16_t);
void tone(uint8_t,uint32_t);
void noTone(uint8_t);
uint32_t pulseIn(uint8_t pin,uint8_t state,uint32_t time=1000000);
uint8_t shiftIn(uint8_t,uint8_t,uint8_t);
void shiftOut(uint8_t,uint8_t,uint8_t,uint8_t);

#endif

/***************************************************************
*Copyright(c) 2020思特诺(Stduino)All right reserved.
*
*This library is open source and free for individual users. 
*
*For commercial use, please contact service001@stduino.com.
***************************************************************/
