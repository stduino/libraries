/***************************************************************
*Copyright(c) 2020思特诺(Stduino)All right reserved.
*
*This library is open source and free for individual users. 
*
*For commercial use, please contact service001@stduino.com.
***************************************************************/

#ifndef StduinoStringFuction_h
#define	StduinoStringFuction_h

#ifdef __cplusplus
extern "C" {
#endif

#include "StduinoMath.h"
#include "StduinoMemory.h"

#define HEX (16)
#define DEC (10)
#define OCT (8)
#define BIN (2)

char *dtostrf(float val, signed char width, unsigned char prec, char *sout);
int _strlen(const char *s);
char *_strcpy(char *dest, const char *src);
int _strcmp(const char *str1, const char *str2);
int _strncmp(const char *str1, const char *str2, unsigned int n);
char *_strncpy(char *dest, const char *src, unsigned int n);
const char *_strchr(const char *str, char c);
char* _strstr(char* src, const char* sub);
char *_strrchr(const char * str,int ch);
int _tolower (int c);
int _toupper(int c);
int _isspace(char c);
long _atol(char *s);
double _atof(const char *str);
char *_utoa (unsigned int val, char *sout, int radix);
char *_ltoa (long val, char *sout, int radix);
char *_itoa (int val, char *sout, int radix);
char *_ultoa (unsigned long val, char *sout, int radix);

#ifdef __cplusplus
}
#endif

#endif

/***************************************************************
*Copyright(c) 2020思特诺(Stduino)All right reserved.
*
*This library is open source and free for individual users. 
*
*For commercial use, please contact service001@stduino.com.
***************************************************************/
