/***************************************************************
*Copyright(c) 2020思特诺(Stduino)All right reserved.
*
*This library is open source and free for individual users. 
*
*For commercial use, please contact service001@stduino.com.
***************************************************************/

#ifndef StduinoTime_h
#define	StduinoTime_h

#ifdef __cplusplus
extern "C" {
#endif

#include "stm32f10x.h"

uint32_t millis(void);
uint32_t micros(void);
void delay(uint32_t);
void delayMicroseconds(uint32_t);

#ifdef __cplusplus
}
#endif

#endif

/***************************************************************
*Copyright(c) 2020思特诺(Stduino)All right reserved.
*
*This library is open source and free for individual users. 
*
*For commercial use, please contact service001@stduino.com.
***************************************************************/
