/***************************************************************
*Copyright(c) 2020思特诺(Stduino)All right reserved.
*
*This library is open source and free for individual users. 
*
*For commercial use, please contact service001@stduino.com.
***************************************************************/

#include "StduinoMath.h"

long long _pow(unsigned int x,unsigned int y)
{
 long long Tx=0;
 for(unsigned int i=0;i<y;i++)
 {
  (Tx==0)?(Tx=(Tx+1)*x):(Tx=Tx*x);
 }
 return (x==0)?0:((Tx==0)?1:Tx);
}

long map(long x,long in_min,long in_max,long out_min,long out_max)
{
 return (((x)-(in_min))*((out_max)-(out_min))/((in_max)-(in_min))+(out_min));
}

// long random(long x)
// {
 // return ;
// }

// long random(long x, long x)
// {
 // return ;
// }

// void randomSeed(unsigned long x)
// {
 // ;
// }

// unsigned int makeWord(unsigned int w)
// {
 // return ;
// }

// unsigned int makeWord(unsigned char h, unsigned char l)
// {
 // return ;
// }

/***************************************************************
*Copyright(c) 2020思特诺(Stduino)All right reserved.
*
*This library is open source and free for individual users. 
*
*For commercial use, please contact service001@stduino.com.
***************************************************************/
