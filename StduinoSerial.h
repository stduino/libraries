/***************************************************************
*Copyright(c) 2020思特诺(Stduino)All right reserved.
*
*This library is open source and free for individual users. 
*
*For commercial use, please contact service001@stduino.com.
***************************************************************/

#ifndef StduinoSerial_h
#define	StduinoSerial_h

#include "StduinoHardwareSerial.h"
#include "StduinoString.h"

class HardwareSerial
{
private:
 uint8_t USARTX;
public:
 HardwareSerial(uint8_t);
 ~HardwareSerial();
 void begin(uint32_t);
 void write(unsigned char);
 void print(unsigned char,unsigned char=DEC);
 void println(unsigned char,unsigned char=DEC);
 void print(unsigned int,unsigned char=DEC);
 void println(unsigned int,unsigned char=DEC);
 void print(unsigned short int,unsigned char=DEC);
 void println(unsigned short int,unsigned char=DEC);
 void print(int,unsigned char=DEC);
 void println(int,unsigned char=DEC);
 void print(long,unsigned char=DEC);
 void println(long,unsigned char=DEC);
 void print(uint32_t,unsigned char=DEC);
 void println(uint32_t,unsigned char=DEC);
 void print(double,unsigned char=2);
 void println(double,unsigned char=2);
 void print(float,unsigned char=1);
 void println(float,unsigned char=1);
 void print(String);
 void println(String);
 void print(const char*);
 void println(const char*);
 void print(char data[]);
 void println(char data[]);
 void print(unsigned char data[]);
 void println(unsigned char data[]);
 void print(void);
 void println(void);
 char read(void);
 uint8_t available(void);
 int peek(void);
 String peekString(void);
 String readString(void);
};
extern HardwareSerial Serial;
extern HardwareSerial Serial1;
extern HardwareSerial Serial2;
#endif 

/***************************************************************
*Copyright(c) 2020思特诺(Stduino)All right reserved.
*
*This library is open source and free for individual users. 
*
*For commercial use, please contact service001@stduino.com.
***************************************************************/
