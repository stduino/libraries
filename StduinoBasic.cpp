/***************************************************************
*Copyright(c) 2020˼��ŵ(Stduino)All right reserved.
*
*This library is open source and free for individual users. 
*
*For commercial use, please contact service001@stduino.com.
***************************************************************/

#include "StduinoBasic.h"

int main()
{
	
 RCC->APB2ENR|=1<<0;
 AFIO->MAPR&=0xF8FFFFFF;
 AFIO->MAPR|=0x02000000;
 RCC->APB2ENR&=~(1<<0);
 
 SysTick->LOAD=(uint32_t)(72-1UL);
 NVIC_SetPriority(SysTick_IRQn,(1UL<<__NVIC_PRIO_BITS)-1UL);
 SysTick->VAL=0UL;
 SysTick->CTRL=SysTick_CTRL_CLKSOURCE_Msk|SysTick_CTRL_TICKINT_Msk|SysTick_CTRL_ENABLE_Msk;
 
 setup();
 
 for(;;)
 {
  loop();
 }
}

/***************************************************************
*Copyright(c) 2020˼��ŵ(Stduino)All right reserved.
*
*This library is open source and free for individual users. 
*
*For commercial use, please contact service001@stduino.com.
***************************************************************/
