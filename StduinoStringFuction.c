/***************************************************************
*Copyright(c) 2020思特诺(Stduino)All right reserved.
*
*This library is open source and free for individual users. 
*
*For commercial use, please contact service001@stduino.com.
***************************************************************/

#include "StduinoStringFuction.h"
char *dtostrf(float val, signed char width, unsigned char prec, char *sout)
{
 unsigned int IsNegative=0;
 int i,t;
 float Data=val;
 
 if(Data<0.0)
 {
  IsNegative=1;
  Data=-Data;
  *sout='-';
 }
 
 unsigned int a=Data;
 
 if(width==0)
 {
  *(sout+IsNegative)='0';
 }
 
 for(i=width;i>0;i--)
 {
  *(sout+IsNegative+width-i)='0'+((a/_pow(10,i-1))-((a/_pow(10,i))*10));
 }
 
 if(prec!=0)
 {
  *(sout+IsNegative+width)='.';
  
  for(t=1;t<=prec;t++)
  {
   long long aq1=Data*_pow(10,t);
   long long aq2=Data*_pow(10,t-1);
   *(sout+IsNegative+width+t)='0'+(aq1-(aq2*10));
  }
  
  *(sout+IsNegative+width+t)='\0';
 }else 
 {
  *(sout+IsNegative+width)='\0';
 }
 
 return sout;
}



int _strlen(const char* str)
{
 const char *temp = str;
 
 while(*str ++ !='\0')
 {
  ;
 }
 
 return str - temp -1;
}

char *_strcpy(char *des,const char *source)
{
 char *r=des;
 
 while((*r++ = *source++)!='\0')
 {
  ;
 }
 
 return des;
}

int _strcmp(const char *str1,const char *str2)
{
 while(*str1 == *str2)
 {
  if(*str1 == '\0')
  {
   return 0;
  }
  
  str1++;
  str2++;
 }
 
 return *str1 - *str2;
}

int _strncmp(const char *s1, const char *s2, unsigned int  len)
{
 while(len--) 
 {
  if(*s1 == 0 || *s1 != *s2)
  {
   return *s1 - *s2;
  }
  
  s1++;
  s2++;
 }
 
 return 0;
}

int _tolower (int c)
{
  return (c >= 'A' && c <= 'Z')?(c - 'A' + 'a'):c;
}

char *_strncpy(char *dest, const char *src, unsigned int count)
{
 unsigned int i;
 
 for(i=0;(i<count)&&(*(src+i)!='\0');i++)
 {
  *(dest+i)=*(src+i);
  *(dest+i+1)='\0';
 }
 
 return dest;
}

const char* _strchr(const char *s, char c)
{
 while(*s != '\0' && *s != c)
 {
  ++s;
 }
 
 return *s==c ? s : 0;
}

char* _strstr(char* src, const char* sub)
{
 const char *bp;
 const char *sp;
 
 if(!src || !sub)
 {
  return src;
 }
 
 while(*src)
 {
  bp = src;
  sp = sub;
  
  do
  {
   if(!*sp)
   {
    return src;
   }
  }while(*bp++ == *sp ++);
  src ++;
 }
 
 return 0;
}

char * _strrchr(const char * str,int ch)
{
 char * start = (char *)str;
 
 while(*str++)
 {
  ;
 }
 
 while(--str != start && *str != (char)ch)
 {
  ;
 }
 
 if(*str == (char)ch)
 {
  return((char *)str);
 }
 
 return 0;
}

int _toupper(int c)
{
  return (c >= 'a') && (c <= 'z')?c = c - ('a' - 'A'):c;
}

int _isspace(char c)
{
 if(c =='\t'|| c =='\n'|| c ==' ')
 {
  return 1;
 }else
 {
  return 0;
 }
}

long _atol(char *s)
{
 long r = 0;
 int neg = 0;
 switch(*s)
 {
  case '-':
   neg = 1;
   
  case '+':
   s++;
   break;
 }
 
 while(*s >= '0' && *s <= '9')
 {
  int n = *s++ - '0';
  
  if(neg)
  {
   n = -n;
  }
  
  r = r * 10 + n;
 }
 
 return r;
}

double _atof(const char *str)
{
 double s=0.0;
 double d=10.0;
 int jishu=0;
 unsigned char flag=0;
 
 while(*str==' ')
 {
  str++;
 }
 
 if(*str=='-')
 {
  flag=1;
  str++;
 }
 
 if(!(*str>='0'&&*str<='9'))
 {
  return s;
 }

 while(*str>='0'&&*str<='9'&&*str!='.')
 {
  s=s*10.0+*str-'0';
  str++;
 }
 
 if(*str=='.')
 {
  str++;
 }
 
 while(*str>='0'&&*str<='9')
 {
  s=s+(*str-'0')/d;
  d*=10.0;
  str++;
 }
 
 if(*str=='e'||*str=='E')
 {
  str++;
  
  if(*str=='+')
  {
   str++;
   
   while(*str>='0'&&*str<='9')
   {
    jishu=jishu*10+*str-'0';
    str++;
   }
   
   while(jishu>0)
   {
    s*=10;
    jishu--;
   }
  }
  
  if(*str=='-')
  {
   str++;
   
   while(*str>='0'&&*str<='9')
   {
    jishu=jishu*10+*str-'0';
    str++;
   }
   
   while(jishu>0)
   {
    s/=10;
    jishu--;
   }
  }
 }
 
 return s*((!!flag)?-1.0:1.0);
}

char *_itoa (int val, char *sout, int radix)
{
 signed int i,t;
 
for(i=0;val==0?(_pow(radix,i)-1)<=val:(_pow(radix,i)-1)<val;i++)
 {
  ;
 }
 
 i--;
 t=i;
 int VALUE=val;
 
 for(;i>=0;i--)
 {
  int x=(VALUE/_pow(radix,i));
  (x>9)?(*(sout+t-i)=(char)(x+55)):(*(sout+t-i)=(char)(x+48));
  VALUE=VALUE-x*_pow(radix,i);
 }
 
 *(sout+t+1)=0;
 return sout;
}

char *_utoa(unsigned int val, char *sout, int radix)
{
 signed int i,t;
 
for(i=0;val==0?(_pow(radix,i)-1)<=val:(_pow(radix,i)-1)<val;i++)
 {
  ;
 }
 
 i--;
 t=i;
 unsigned int VALUE=val;
 
 for(;i>=0;i--)
 {
  unsigned int x=(VALUE/_pow(radix,i));
  (x>9)?(*(sout+t-i)=(char)(x+55)):(*(sout+t-i)=(char)(x+48));
  VALUE=VALUE-x*_pow(radix,i);
 }
 
 *(sout+t+1)=0;
 return sout;
 
}

char *_ltoa (long val, char *sout, int radix)
{
 signed int i,t;
 
for(i=0;val==0?(_pow(radix,i)-1)<=val:(_pow(radix,i)-1)<val;i++)
 {
  ;
 }
 
 i--;
 t=i;
 long VALUE=val;
 
 for(;i>=0;i--)
 {
  long x=(VALUE/_pow(radix,i));
  (x>9)?(*(sout+t-i)=(char)(x+55)):(*(sout+t-i)=(char)(x+48));
  VALUE=VALUE-x*_pow(radix,i);
 }
 
 *(sout+t+1)=0;
 return sout;
 
}

char *_ultoa (unsigned long val, char *sout, int radix)
{
 signed int i,t;
 
for(i=0;val==0?(_pow(radix,i)-1)<=val:(_pow(radix,i)-1)<val;i++)
 {
  ;
 }
 
 i--;
 t=i;
 unsigned long VALUE=val;
 
 for(;i>=0;i--)
 {
  unsigned long x=(VALUE/_pow(radix,i));
  (x>9)?(*(sout+t-i)=(char)(x+55)):(*(sout+t-i)=(char)(x+48));
  VALUE=VALUE-x*_pow(radix,i);
 }
 
 *(sout+t+1)=0;
 return sout;
}

/***************************************************************
*Copyright(c) 2020思特诺(Stduino)All right reserved.
*
*This library is open source and free for individual users. 
*
*For commercial use, please contact service001@stduino.com.
***************************************************************/
