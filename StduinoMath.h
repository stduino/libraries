/***************************************************************
*Copyright(c) 2020思特诺(Stduino)All right reserved.
*
*This library is open source and free for individual users. 
*
*For commercial use, please contact service001@stduino.com.
***************************************************************/

#ifndef StduinoMath_h
#define	StduinoMath_h

#ifdef __cplusplus
extern "C" {
#endif

#include "Stduino.h"
#define Pi 3.14159265358979323846
#define lowByte(w) ((uint8_t) ((w) & 0xff))
#define highByte(w) ((uint8_t) ((w) >> 8))
#define bitRead(value, bit) (((value) >> (bit)) & 0x01)
#define bitSet(value, bit) ((value) |= (1UL << (bit)))
#define bitClear(value, bit) ((value) &= ~(1UL << (bit)))
#define bitWrite(value, bit, bitvalue) (bitvalue ? bitSet(value, bit) : bitClear(value, bit))
#define bit(b) (1UL << (b))
#define abs(x) ((x)>0?(x):-(x))
#define constrain(amt,low,high) ((amt)<(low)?(low):((amt)>(high)?(high):(amt)))

#define max(a,b) ((a)>(b)?(a):(b))
#define min(a,b) ((a)<(b)?(a):(b))
#define sq(x) ((x)*(x))

long long _pow(unsigned int x,unsigned int y);
long map(long, long, long, long, long);
// long random(long);
// long random(long,long);
// void randomSeed(unsigned long);
// unsigned int makeWord(unsigned int w);
// unsigned int makeWord(unsigned char h, unsigned char l);
#ifdef __cplusplus
}
#endif

#endif

/***************************************************************
*Copyright(c) 2020思特诺(Stduino)All right reserved.
*
*This library is open source and free for individual users. 
*
*For commercial use, please contact service001@stduino.com.
***************************************************************/
