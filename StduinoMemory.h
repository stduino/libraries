/***************************************************************
*Copyright(c) 2020思特诺(Stduino)All right reserved.
*
*This library is open source and free for individual users. 
*
*For commercial use, please contact service001@stduino.com.
***************************************************************/

#ifndef StduinoMemory_h
#define	StduinoMemory_h

#ifdef __cplusplus
extern "C" {
#endif

#define BlockSize (32)
#define MemorySize (800*2)
#define MemoryMapSize (MemorySize/BlockSize)

struct STDUINOMEMORY
{
void (*Begin)(void);
unsigned char (*MemoryUsed)(void);
unsigned char  MemoryBasic[MemorySize];
unsigned short MemoryMap[MemoryMapSize];
unsigned char  MemoryReadyFlag;
};

extern struct STDUINOMEMORY StduinoMemory;
void MEMCPY(void *des,void *src,unsigned long len);
void MEMSET(void *s,unsigned char c,unsigned long count);
void *MEMMOVE( void *dest , const void *src , unsigned int count);
void INIT(void);
unsigned long MALLOC(unsigned long size);
unsigned char FREE(unsigned long offset);
unsigned char PERUSED(void);
void Stduino_free(void *ptr);
void *Stduino_malloc(unsigned long size);
void *Stduino_realloc(void *ptr,unsigned long size);

#ifdef __cplusplus
}
#endif

#endif

/***************************************************************
*Copyright(c) 2020思特诺(Stduino)All right reserved.
*
*This library is open source and free for individual users. 
*
*For commercial use, please contact service001@stduino.com.
***************************************************************/
