# stduinolibraries

#### 介绍
Stduino libraries 官方库 ，官方网站www.stduino.com

#### 架构
本函数库大部分直接基于寄存器进行开发，目前已基本完成STM32F103中等容量芯片的支持，现因学业繁忙，接下来几个月没时间进行继续开发，遂暂停一段时间对本函数库的维护更新，在此期间为不影响Stduino IDE正常使用，现决定自软件1.01版本起采用Arduino_Core_STM32框架进行开发。

若您有兴趣基于本函数库继续开发，可下载Stduino IDE1.0版本进行测试开发。
[Stduino IDE 1.0版软件](https://pan.bnu.edu.cn/l/I510ph) 密码：qija 

#### 软件使用教程

1. 解压后双击Stduino.exe即可启动


#### 使用说明

1. 编译
2. 下载
3. 串口调试

#### 参与贡献

1. Fork 本仓库
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request


#### 码云特技

1. 使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2. 码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3. 你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4. [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5. 码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6. 码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)