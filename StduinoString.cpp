/***************************************************************
*Copyright(c) 2020思特诺(Stduino)All right reserved.
*
*This library is open source and free for individual users. 
*
*For commercial use, please contact service001@stduino.com.
***************************************************************/

#include "StduinoString.h"

String::String(const char *cstr)
{
 init();

 if (cstr)
 {
  copy(cstr, _strlen(cstr));
 }
}

String::String(const String &value)
{
 init();
 *this = value;
}

String::String(char c)
{
 init();
 char buf[2];
 buf[0] = c;
 buf[1] = 0;
 *this = buf;
}

String::String(unsigned char value, unsigned char base)
{
 init();
 char buf[1 + 8 * sizeof(unsigned char)];
 _utoa(value, buf, base);
 *this = buf;
}

String::String(int value, unsigned char base)
{
 init();
 char buf[2 + 8 * sizeof(int)];
 _itoa(value, buf, base);
 *this = buf;
}

String::String(unsigned int value, unsigned char base)
{
 init();
 char buf[1 + 8 * sizeof(unsigned int)];
 _utoa(value, buf, base);
 *this = buf;
}

String::String(long value, unsigned char base)
{
 init();
 char buf[2 + 8 * sizeof(long)];
 _ltoa(value, buf, base);
 *this = buf;
}

String::String(unsigned long value, unsigned char base)
{
 init();
 char buf[1 + 8 * sizeof(unsigned long)];
 _ultoa(value, buf, base);
 *this = buf;
}

String::String(float value, unsigned char decimalPlaces)
{
 init();
 
 char buf[33];
 long long length=0;
 unsigned char i=0;
 
 for(length=0;length<=value;(length==0)?(length=(length+1)*10):(length=length*10))
 {
  i++;
 }
 
 *this = dtostrf(value, i, decimalPlaces, buf);
}

String::String(double value, unsigned char decimalPlaces)
{
 init();
 
 char buf[33];
 long long length=0;
 unsigned char i=0;

 for(length=0;length<=value;(length==0)?(length=(length+1)*10):(length=length*10))
 {
  i++;
 }
 
 *this = dtostrf(value, i, decimalPlaces, buf);
}

String::~String()
{
 Stduino_free(buffer);
}

inline void String::init(void)
{
 buffer = (char*)Stduino_malloc(2);
 capacity = 0;
 len = 0;
}

void String::invalidate(void)
{
 if(buffer)
  Stduino_free(buffer);
 buffer = 0;
 capacity = len = 0;
}

unsigned char String::reserve(unsigned int size)
{
 if (buffer && capacity >= size)
 {
  return 1;
 }

 if (changeBuffer(size))
 {
  if (len == 0) 
  {
   buffer[0] = 0;
  }
   
  return 1;
 }

 return 0;
}

unsigned char String::changeBuffer(unsigned int maxStrLen)
{
 char *newbuffer = (char *)Stduino_realloc(buffer, maxStrLen + 1);

 if(newbuffer)
 {
  buffer = newbuffer;
  capacity = maxStrLen;
  return 1;
 }

 return 0;
}

String & String::copy(const char *cstr, unsigned int length)
{
 if (!reserve(length))
 {
  invalidate();
  return *this;
 }

 len = length;
 _strcpy(buffer, cstr);
 return *this;
}

String & String::operator = (const String &rhs)
{
 if (this == &rhs)
 {
  return *this;
 }

 if (rhs.buffer)
 {
  copy(rhs.buffer, rhs.len);
 }else
 {
  invalidate();
 }

 return *this;
}

String & String::operator = (const char *cstr)
{
 if (cstr)
 {
  copy(cstr, _strlen(cstr));
 }else
 {
  invalidate();
 }

 return *this;
}

unsigned char String::concat(const String &s)
{
 return concat(s.buffer, s.len);
}

unsigned char String::concat(const char *cstr, unsigned int length)
{
 unsigned int newlen = len + length;

 if (!cstr)
 {
  return 0;
 }

 if (length == 0)
 {
  return 1;
 }

 if (!reserve(newlen))
 {
  return 0;
 }

 _strcpy(buffer + len, cstr);
 len = newlen;
 return 1;
}

unsigned char String::concat(const char *cstr)
{
 if (!cstr)
 {
  return 0;
 }

 return concat(cstr, _strlen(cstr));
}

unsigned char String::concat(char c)
{
 char buf[2];
 buf[0] = c;
 buf[1] = 0;
 return concat(buf, 1);
}

unsigned char String::concat(unsigned char num)
{
 char buf[1 + 3 * sizeof(unsigned char)];
 _itoa(num, buf, 10);
 return concat(buf, _strlen(buf));
}

unsigned char String::concat(int num)
{
 char buf[2 + 3 * sizeof(int)];
 _itoa(num, buf, 10);
 return concat(buf, _strlen(buf));
}

unsigned char String::concat(unsigned int num)
{
 char buf[1 + 3 * sizeof(unsigned int)];
 _utoa(num, buf, 10);
 return concat(buf, _strlen(buf));
}

unsigned char String::concat(long num)
{
 char buf[2 + 3 * sizeof(long)];
 _ltoa(num, buf, 10);
 return concat(buf, _strlen(buf));
}

unsigned char String::concat(unsigned long num)
{
 char buf[1 + 3 * sizeof(unsigned long)];
 _ultoa(num, buf, 10);
 return concat(buf, _strlen(buf));
}

unsigned char String::concat(float num)
{
 char buf[20];
 char* string = dtostrf(num, 4, 2, buf);
 return concat(string, _strlen(string));
}

unsigned char String::concat(double num)
{
 char buf[20];
 char* string = dtostrf(num, 4, 2, buf);
 return concat(string, _strlen(string));
}

StringSumHelper & operator + (const StringSumHelper &lhs, const String &rhs)
{
 StringSumHelper &a = const_cast<StringSumHelper&>(lhs);

 if (!a.concat(rhs.buffer, rhs.len))
 {
  a.invalidate();
 }

 return a;
}

StringSumHelper & operator + (const StringSumHelper &lhs, const char *cstr)
{
 StringSumHelper &a = const_cast<StringSumHelper&>(lhs);

 if (!cstr || !a.concat(cstr, _strlen(cstr)))
 {
  a.invalidate();
 }

 return a;
}

StringSumHelper & operator + (const StringSumHelper &lhs, char c)
{
 StringSumHelper &a = const_cast<StringSumHelper&>(lhs);

 if (!a.concat(c))
 {
  a.invalidate();
 }

 return a;
}

StringSumHelper & operator + (const StringSumHelper &lhs, unsigned char num)
{
 StringSumHelper &a = const_cast<StringSumHelper&>(lhs);

 if (!a.concat(num))
 {
  a.invalidate();
 }

 return a;
}

StringSumHelper & operator + (const StringSumHelper &lhs, int num)
{
 StringSumHelper &a = const_cast<StringSumHelper&>(lhs);

 if (!a.concat(num))
 {
  a.invalidate();
 }

 return a;
}

StringSumHelper & operator + (const StringSumHelper &lhs, unsigned int num)
{
 StringSumHelper &a = const_cast<StringSumHelper&>(lhs);

 if (!a.concat(num))
 {
  a.invalidate();
 }

 return a;
}

StringSumHelper & operator + (const StringSumHelper &lhs, long num)
{
 StringSumHelper &a = const_cast<StringSumHelper&>(lhs);

 if (!a.concat(num))
 {
  a.invalidate();
 }

 return a;
}

StringSumHelper & operator + (const StringSumHelper &lhs, unsigned long num)
{
 StringSumHelper &a = const_cast<StringSumHelper&>(lhs);

 if (!a.concat(num))
 {
  a.invalidate();
 }

 return a;
}

StringSumHelper & operator + (const StringSumHelper &lhs, float num)
{
 StringSumHelper &a = const_cast<StringSumHelper&>(lhs);

 if (!a.concat(num))
 {
  a.invalidate();
 }

 return a;
}

StringSumHelper & operator + (const StringSumHelper &lhs, double num)
{
 StringSumHelper &a = const_cast<StringSumHelper&>(lhs);

 if (!a.concat(num))
 {
  a.invalidate();
 }

 return a;
}

int String::compareTo(const String &s) const
{
 if (!buffer || !s.buffer)
 {
  if (s.buffer && s.len > 0)
  {
   return 0 - *(unsigned char *)s.buffer;
  }

  if (buffer && len > 0)
  {
   return *(unsigned char *)buffer;
  }

  return 0;
 }

 return _strcmp(buffer, s.buffer);
}

unsigned char String::equals(const String &s2) const
{
 return (len == s2.len && compareTo(s2) == 0);
}

unsigned char String::equals(const char *cstr) const
{
 if (len == 0) 
 {
  return (cstr == 0 || *cstr == 0);
 }

 if (cstr == 0) 
 {
  return buffer[0] == 0;
 }

 return _strcmp(buffer, cstr) == 0;
}

unsigned char String::operator<(const String &rhs) const
{
 return compareTo(rhs) < 0;
}

unsigned char String::operator>(const String &rhs) const
{
 return compareTo(rhs) > 0;
}

unsigned char String::operator<=(const String &rhs) const
{
 return compareTo(rhs) <= 0;
}

unsigned char String::operator>=(const String &rhs) const
{
 return compareTo(rhs) >= 0;
}

unsigned char String::equalsIgnoreCase( const String &s2 ) const
{
 if (this == &s2)
 {
  return 1;
 }

 if (len != s2.len)
 {
  return 0;
 }

 if (len == 0)
 {
  return 1;
 }

 const char *p1 = buffer;
 const char *p2 = s2.buffer;

 while (*p1)
 {
  if (_tolower(*p1++) != _tolower(*p2++))
  {
   return 0;
  }
 } 
 return 1;
}

unsigned char String::startsWith( const String &s2 ) const
{
 if (len < s2.len)
 {
  return 0;
 }

 return startsWith(s2, 0);
}

unsigned char String::startsWith( const String &s2, unsigned int offset ) const
{
 if (offset > len - s2.len || !buffer || !s2.buffer)
 {
  return 0;
 }

 return _strncmp( &buffer[offset], s2.buffer, s2.len ) == 0;
}

unsigned char String::endsWith( const String &s2 ) const
{
 if ( len < s2.len || !buffer || !s2.buffer)
 {
  return 0;
 }

 return _strcmp(&buffer[len - s2.len], s2.buffer) == 0;
}

char String::charAt(unsigned int loc) const
{
 return operator[](loc);
}

void String::setCharAt(unsigned int loc, char c) 
{
 if (loc < len)
 {
  buffer[loc] = c;
 }
}

char & String::operator[](unsigned int index)
{
 static char dummy_writable_char;

 if (index >= len || !buffer)
 {
  dummy_writable_char = 0;
  return dummy_writable_char;
 }

 return buffer[index];
}

char String::operator[]( unsigned int index ) const
{
 if (index >= len || !buffer)
 {
  return 0;
 }
 return buffer[index];
}

void String::getBytes(unsigned char *buf, unsigned int bufsize, unsigned int index) const
{
 if (!bufsize || !buf)
 {
  return ;
 }

 if (index >= len)
 {
  buf[0] = 0;
  return ;
 }

 unsigned int n = bufsize - 1;

 if (n > len - index)
 {
  n = len - index;
 }

 _strncpy((char *)buf, (buffer + index), n);
 buf[n] = 0;
}

int String::indexOf(char c) const
{
 return indexOf(c, 0);
}

int String::indexOf( char ch, unsigned int fromIndex ) const
{
 if (fromIndex >= len)
 {
  return -1;
 }

 const char* temp = _strchr(buffer + fromIndex, ch);

 if (temp == 0)
 {
  return -1;
 }

 return temp - buffer;
}

int String::indexOf(const String &s2) const
{
 return indexOf(s2, 0);
}

int String::indexOf(const String &s2, unsigned int fromIndex) const
{
 if (fromIndex >= len)
 {
  return -1;
 }

 const char *found = _strstr(buffer + fromIndex, s2.buffer);

 if (found == 0)
 {
  return -1;
 }

 return found - buffer;
 
}

int String::lastIndexOf( char theChar ) const
{
 return lastIndexOf(theChar, len - 1);
}

int String::lastIndexOf(char ch, unsigned int fromIndex) const
{
 if (fromIndex >= len)
 {
  return -1;
 }

 char tempchar = buffer[fromIndex + 1];
 buffer[fromIndex + 1] = '\0';
 char* temp = _strrchr( buffer, ch );
 buffer[fromIndex + 1] = tempchar;

 if (temp == 0)
 {
  return -1;
 }

 return temp - buffer;
}

int String::lastIndexOf(const String &s2) const
{
 return lastIndexOf(s2, len - s2.len);
}

int String::lastIndexOf(const String &s2, unsigned int fromIndex) const
{
 if (s2.len == 0 || len == 0 || s2.len > len)
 {
  return -1;
 }

 if (fromIndex >= len)
 {
  fromIndex = len - 1;
 }

 int found = -1;

 for (char *p = buffer; p <= buffer + fromIndex; p++)
 {
  p = _strstr(p, s2.buffer);

  if (!p)
  {
   break;
  }

  if ((unsigned int)(p - buffer) <= fromIndex)
  {
   found = p - buffer;
  }
 }

 return found;
}

String String::substring(unsigned int left, unsigned int right) const
{
 if (left > right)
 {
  unsigned int temp = right;
  right = left;
  left = temp;
 }

 String out;

 if (left >= len)
 {
  return out;
 }

 if (right > len)
 {
  right = len;
 }

 char temp = buffer[right];
 buffer[right] = '\0';	
 out = buffer + left;
 buffer[right] = temp;
 return out;
}

void String::replace(char find, char replace)
{
 if (!buffer)
 {
  return ;
 }

 for (char *p = buffer; *p; p++)
 {
  if (*p == find)
  {
   *p = replace;
  }
 }
}

void String::replace(const String& find, const String& replace)
{
 if (len == 0 || find.len == 0)
 {
  return ;
 }

 int diff = replace.len - find.len;
 char *readFrom = buffer;
 char *foundAt;

 if (diff == 0)
 {
  while ((foundAt = _strstr(readFrom, find.buffer)) != 0)
  {
   MEMCPY(foundAt, replace.buffer, replace.len);
   readFrom = foundAt + replace.len;
  }
 }else if (diff < 0) 
 {
  char *writeTo = buffer;

  while ((foundAt = _strstr(readFrom, find.buffer)) != 0)
  {
   unsigned int n = foundAt - readFrom;
   MEMCPY(writeTo, readFrom, n);
   writeTo += n;
   MEMCPY(writeTo, replace.buffer, replace.len);
   writeTo += replace.len;
   readFrom = foundAt + find.len;
   len += diff;
  }

  _strcpy(writeTo, readFrom);
 }else 
 {
  unsigned int size = len;

  while ((foundAt = _strstr(readFrom, find.buffer)) != 0)
  {
   readFrom = foundAt + find.len;
   size += diff;
  }

  if (size == len)
  {
   return;
  }

  if (size > capacity && !changeBuffer(size))
  {
   return;
  }

  int index = len - 1;

  while (index >= 0 && (index = lastIndexOf(find, index)) >= 0)
  {
   readFrom = buffer + index + find.len;
   MEMMOVE(readFrom + diff, readFrom, len - (readFrom - buffer));
   len += diff;
   buffer[len] = 0;
   MEMCPY(buffer + index, replace.buffer, replace.len);
   index--;
  }
 }
}

void String::remove(unsigned int index)
{
 remove(index, (unsigned int)-1);
}

void String::remove(unsigned int index, unsigned int count)
{
 if (index >= len)
 {
  return ;
 }

 if (count <= 0)
 {
  return ;
 }

 if (count > len - index)
 {
  count = len - index;
 }

 char *writeTo = buffer + index;
 len = len - count;
 _strncpy(writeTo, buffer + index + count,len - index);
 buffer[len] = 0;
}

void String::toLowerCase(void)
{
 if(!buffer)
 {
  return ;
 }

 for (char *p = buffer; *p; p++)
 {
  *p = _tolower(*p);
 }
}

void String::toUpperCase(void)
{
 if(!buffer)
 {
  return ;
 }

 for(char *p = buffer; *p; p++)
 {
  *p = _toupper(*p);
 }
}

void String::trim(void)
{
 if(!buffer || len == 0)
 {
  return ;
 }

 char *begin = buffer;

 while(_isspace(*begin))
 {
  begin++;
 }

 char *end = buffer + len - 1;

 while(_isspace(*end) && end >= begin)
 {
  end--;
 }
 len = end + 1 - begin;

 if(begin > buffer)
 {
  MEMCPY(buffer, begin, len);
 }

 buffer[len] = 0;
}

long String::toInt(void) const
{
 if(buffer)
 {
  return _atol(buffer);
 }

 return 0;
}

float String::toFloat(void) const
{
 return float(toDouble());
}

double String::toDouble(void) const
{
 if(buffer)
 {
  return float(_atof(buffer));
 }

 return 0;
}

/***************************************************************
*Copyright(c) 2020思特诺(Stduino)All right reserved.
*
*This library is open source and free for individual users. 
*
*For commercial use, please contact service001@stduino.com.
***************************************************************/
