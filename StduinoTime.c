/***************************************************************
*Copyright(c) 2020思特诺(Stduino)All right reserved.
*
*This library is open source and free for individual users. 
*
*For commercial use, please contact service001@stduino.com.
***************************************************************/

#include "StduinoTime.h"

volatile uint32_t _time_us=0;
volatile uint32_t _time_u=0;
volatile uint32_t _time_m=0;

void SysTick_Handler(void)
{
 _time_u++;
 if(_time_u>=1000)
 {
  _time_u-=1000;
  _time_m++;
 }
 if(_time_us)
 {
  _time_us--;
 }
}

uint32_t millis(void)
{
 return _time_m;
}

uint32_t micros(void)
{
 return (_time_m*1000+_time_u);
}

void delay(uint32_t ms)
{
 delayMicroseconds(ms*1000);
}

void delayMicroseconds(uint32_t us)
{
 _time_us=us;
 while(_time_us);
}

/***************************************************************
*Copyright(c) 2020思特诺(Stduino)All right reserved.
*
*This library is open source and free for individual users. 
*
*For commercial use, please contact service001@stduino.com.
***************************************************************/
