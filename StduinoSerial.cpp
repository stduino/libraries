/***************************************************************
*Copyright(c) 2020思特诺(Stduino)All right reserved.
*
*This library is open source and free for individual users. 
*
*For commercial use, please contact service001@stduino.com.
***************************************************************/

#include "StduinoSerial.h"

HardwareSerial Serial(_usart_a);
HardwareSerial Serial1(_usart_b);
HardwareSerial Serial2(_usart_c);

HardwareSerial::HardwareSerial(uint8_t usartx)
{
 USARTX=usartx;
}

HardwareSerial::~HardwareSerial()
{
 ;
}

void HardwareSerial::begin(uint32_t baud)
{
 Serial_begin((uint8_t)USARTX,baud);
}

void HardwareSerial::write(unsigned char data)
{
 Serial_write((uint8_t)USARTX,data);
}

void HardwareSerial::print(unsigned char data,unsigned char system)
{
 unsigned int Data=data;
 String data_s="";
 data_s+=String((unsigned int)Data,system);
 print((String)data_s);
}

void HardwareSerial::println(unsigned char data,unsigned char system)
{
 print((unsigned int)data,system);
 write('\r');
 write('\n');
}

void HardwareSerial::print(unsigned int data,unsigned char system)
{
 unsigned int Data=data;
 String data_s="";
 data_s+=String((unsigned int)Data,system);
 print((String)data_s);
}

void HardwareSerial::println(unsigned int data,unsigned char system)
{
 print((unsigned int)data,system);
 write('\r');
 write('\n');
}

void HardwareSerial::print(unsigned short int data,unsigned char system)
{
 unsigned int Data=data;
 String data_s="";
 data_s+=String((unsigned int)Data,system);
 print((String)data_s);
}

void HardwareSerial::println(unsigned short int data,unsigned char system)
{
 print((unsigned short int)data,system);
 write('\r');
 write('\n');
}

void HardwareSerial::print(int data,unsigned char system)
{
 int Data=data;
 String data_s="";
 if(data<0)
 {
  Data=-Data;
  data_s+="-";
 }
 data_s+=String((int)Data,system);
 print((String)data_s);
}

void HardwareSerial::println(int data,unsigned char system)
{
 print((int)data,system);
 write('\r');
 write('\n');
}

void HardwareSerial::print(long data,unsigned char system)
{
 print((unsigned int)data,system);
}
void HardwareSerial::println(long data,unsigned char system)
{
 print((unsigned int)data,system);
 write('\r');
 write('\n');
}
void HardwareSerial::print(uint32_t data,unsigned char system)
{
 print((unsigned int)data,system);
}

void HardwareSerial::println(uint32_t data,unsigned char system)
{
 print((unsigned int)data,system);
 write('\r');
 write('\n');
}

void HardwareSerial::print(double data,unsigned char decimalPlaces)
{
 double Data=data;
 String data_s="";
 data_s=String(Data,decimalPlaces);
 print((String)data_s);
}

void HardwareSerial::println(double data,unsigned char decimalPlaces)
{
 print(data);
 write('\r');
 write('\n');
}

void HardwareSerial::print(float data,unsigned char decimalPlaces)
{
 float Data=data;
 String data_s="";
 data_s=String(Data,decimalPlaces);
 print((String)data_s);
}

void HardwareSerial::println(float data,unsigned char decimalPlaces)
{
 print(data,decimalPlaces);
 write('\r');
 write('\n');
}

void HardwareSerial::print(String data)
{
 uint8_t i=0;
 
 for(i=0;i<data.length();i++)
 {
  write(data[i]);
 }
}

void HardwareSerial::println(String data)
{
 print(data);
 write('\r');
 write('\n');
}

void HardwareSerial::print(const char* data)
{
 signed int i=0;
 for(i=0;i<_strlen(data);i++)
 {
  write(*(data+i));
 }
}

void HardwareSerial::println(const char* data)
{
 print((const char*)data);
 write('\r');
 write('\n');
}

void HardwareSerial::print(char data[])
{
 for(unsigned int i=0;data[i]!='\0';i++)
 {
  write(data[i]);
 }
}

void HardwareSerial::println(char data[])
{
 print(data);
 write('\r');
 write('\n');
}

void HardwareSerial::print(unsigned char data[])
{
 for(unsigned int i=0;data[i]!='\0';i++)
 {
  write(data[i]);
 }
}

void HardwareSerial::println(unsigned char data[])
{
 print(data);
 write('\r');
 write('\n');
}

void HardwareSerial::print(void)
{
 ;
}
void HardwareSerial::println(void)
{
 write('\r');
 write('\n');
}

char HardwareSerial::read(void)
{
 return Serial_read((uint8_t)USARTX);
}

uint8_t HardwareSerial::available(void)
{
 return Serial_available((uint8_t)USARTX);
}

int HardwareSerial::peek(void)
{
 char ch=Serial_peek((uint8_t)USARTX);
 if (ch=='\0')
 {
  return -1;
 }else
 {
  return ch;
 }
}

String HardwareSerial::peekString(void)
{
 String S="";
 char *C=Serial_peekString((uint8_t)USARTX);
 
 for(uint8_t i=0;i<_strlen(C);i++)
 {
  S+=*(C+i);
 }
 
 return S;
}

String HardwareSerial::readString(void)
{
 String S="";
 char *C=Serial_readString((uint8_t)USARTX);
 
 for(uint8_t i=0;i<_strlen(C);i++)
 {
  S+=*(C+i);
 }
 
 return S;
}

/***************************************************************
*Copyright(c) 2020思特诺(Stduino)All right reserved.
*
*This library is open source and free for individual users. 
*
*For commercial use, please contact service001@stduino.com.
***************************************************************/
