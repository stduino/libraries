/***************************************************************
*Copyright(c) 2020思特诺(Stduino)All right reserved.
*
*This library is open source and free for individual users. 
*
*For commercial use, please contact server001@stduino.com.
***************************************************************/

#ifndef STDUINO_h
#define	STDUINO_h

#ifdef __cplusplus
extern "C" {
#endif

#include "stm32f10x.h"
#include "StduinoTime.h"

#define LOW 0
#define	HIGH !LOW
#define	false 0
#define true !false

#define boolean bool

#define _gpio_a 0x00
#define _gpio_b 0x01
#define _gpio_c 0x02
#define _gpio_d 0x03
#define _gpio_e 0x04
#define _gpio_f 0x05
#define _gpio_g 0x06
#define _gpio_0 0x00
#define _gpio_1 0x01
#define _gpio_2 0x02
#define _gpio_3 0x03
#define _gpio_4 0x04
#define _gpio_5 0x05
#define _gpio_6 0x06
#define _gpio_7 0x07
#define _gpio_8 0x08
#define _gpio_9 0x09
#define _gpio_10 0x0A
#define _gpio_11 0x0B
#define _gpio_12 0x0C
#define _gpio_13 0x0D
#define _gpio_14 0x0E
#define _gpio_15 0x0F

#define _adc_a 0x24
#define _adc_b 0x28
#define _adc_c 0x3C

#define _tim_a 0x012C
#define _tim_b 0x0000
#define _tim_c 0x0004
#define _tim_d 0x0008
#define _tim_1 0x01
#define _tim_2 0x02
#define _tim_3 0x03
#define _tim_4 0x04

#define _dma_a 0x08
#define _dma_b 0x1C
#define _dma_c 0x30
#define _dma_d 0x44
#define _dma_e 0x58
#define _dma_f 0x6C
#define _dma_g 0x80

#define _usart_a 0x00
#define _usart_b 0x01
#define _usart_c 0x02

#define IP_AN 0x00
#define IP_FL 0x04
#define IP_DW 0x08
#define IP_UP 0x08
#define OP_PP_10 0x01
#define OP_OD_10 0x05
#define OP_AP_10 0x09
#define OP_AD_10 0x0D
#define OP_PP_2 0x02
#define OP_OD_2 0x06
#define OP_AP_2 0x0A
#define OP_AD_2 0x0E
#define OP_PP_50 0x03
#define OP_OD_50 0x07
#define OP_AP_50 0x0B
#define OP_AD_50 0x0F

#define OUTPUT 0x01
#define INPUT 0x02
#define INPUT_PULLUP 0x03
#define INPUT_PULLDW 0x04
#define INPUT_ANALOG 0x05
#define OUTPUT_PULSE 0x06
#define INPUT_PULSE 0x07

#if defined (STM32F10X_MD)
static const uint8_t PA9=0;
static const uint8_t PA10=1;
static const uint8_t PA2=2;
static const uint8_t PA3=3;
static const uint8_t PB10=4;
static const uint8_t PB11=5;
static const uint8_t PA8=6;
static const uint8_t PA11=7;
static const uint8_t PA12=8;
static const uint8_t PB6=9;
static const uint8_t PB7=10;
static const uint8_t PB8=11;
static const uint8_t PB9=12;
static const uint8_t PA15=13;
static const uint8_t PB3=14;
static const uint8_t PB4=15;
static const uint8_t PB5=16;
static const uint8_t PB12=17;
static const uint8_t PB13=18;
static const uint8_t PB14=19;
static const uint8_t PB15=20;
static const uint8_t PC13=21;
static const uint8_t PC6=22;
static const uint8_t PC7=23;
static const uint8_t PC8=24;
static const uint8_t PC9=25;
static const uint8_t PC10=26;
static const uint8_t PC11=27;
static const uint8_t PC12=28;
static const uint8_t PD2=29;
static const uint8_t PE2=30;
static const uint8_t PE3=31;
static const uint8_t PE4=32;
static const uint8_t PE5=33;
static const uint8_t PE6=34;
static const uint8_t PE7=35;
static const uint8_t PE8=36;
static const uint8_t PE9=37;
static const uint8_t PE10=38;
static const uint8_t PE11=39;
static const uint8_t PE12=40;
static const uint8_t PE13=41;
static const uint8_t PE14=42;
static const uint8_t PE15=43;
static const uint8_t PD8=44;
static const uint8_t PD9=45;
static const uint8_t PD10=46;
static const uint8_t PD11=47;
static const uint8_t PD12=48;
static const uint8_t PD13=49;
static const uint8_t PD14=50;
static const uint8_t PD15=51;
static const uint8_t PD3=52;
static const uint8_t PD4=53;
static const uint8_t PD5=54;
static const uint8_t PD6=55;
static const uint8_t PD7=56;
static const uint8_t PE0=57;
static const uint8_t PE1=58;
static const uint8_t PD0=59;
static const uint8_t PD1=60;
static const uint8_t PA0=61;
static const uint8_t PA1=62;
static const uint8_t PA4=63;
static const uint8_t PA5=64;
static const uint8_t PA6=65;
static const uint8_t PA7=66;
static const uint8_t PB0=67;
static const uint8_t PB1=68;
static const uint8_t PC0=69;
static const uint8_t PC1=70;
static const uint8_t PC2=71;
static const uint8_t PC3=72;
static const uint8_t PC4=73;
static const uint8_t PC5=74;


static const uint8_t A0 = 61;
static const uint8_t A1 = 62;
static const uint8_t A2 = 63;
static const uint8_t A3 = 64;
static const uint8_t A4 = 65;
static const uint8_t A5 = 66;
static const uint8_t A6 = 67;
static const uint8_t A7 = 68;
static const uint8_t A8 = 69;
static const uint8_t A9 = 70;
static const uint8_t A10 = 71;
static const uint8_t A11 = 72;
static const uint8_t A12 = 73;
static const uint8_t A13 = 74;
static const uint8_t _gpio_group[]=
{
_gpio_a,_gpio_a,_gpio_a,_gpio_a,_gpio_b,
_gpio_b,_gpio_a,_gpio_a,_gpio_a,_gpio_b,
_gpio_b,_gpio_b,_gpio_b,_gpio_a,_gpio_b,
_gpio_b,_gpio_b,_gpio_b,_gpio_b,_gpio_b,
_gpio_b,_gpio_c,_gpio_c,_gpio_c,_gpio_c,
_gpio_c,_gpio_c,_gpio_c,_gpio_c,_gpio_d,
_gpio_e,_gpio_e,_gpio_e,_gpio_e,_gpio_e,
_gpio_e,_gpio_e,_gpio_e,_gpio_e,_gpio_e,
_gpio_e,_gpio_e,_gpio_e,_gpio_e,_gpio_d,
_gpio_d,_gpio_d,_gpio_d,_gpio_d,_gpio_d,
_gpio_d,_gpio_d,_gpio_d,_gpio_d,_gpio_d,
_gpio_d,_gpio_d,_gpio_e,_gpio_e,_gpio_d,
_gpio_d,_gpio_a,_gpio_a,_gpio_a,_gpio_a,
_gpio_a,_gpio_a,_gpio_b,_gpio_b,_gpio_c,
_gpio_c,_gpio_c,_gpio_c,_gpio_c,_gpio_c,
};
static const uint8_t _gpio_port[]=
{
_gpio_9,_gpio_10,_gpio_2,_gpio_3,_gpio_10,
_gpio_11,_gpio_8,_gpio_11,_gpio_12,_gpio_6,
_gpio_7,_gpio_8,_gpio_9,_gpio_15,_gpio_3,
_gpio_4,_gpio_5,_gpio_12,_gpio_13,_gpio_14,
_gpio_15,_gpio_13,_gpio_6,_gpio_7,_gpio_8,
_gpio_9,_gpio_10,_gpio_11,_gpio_12,_gpio_2,
_gpio_2,_gpio_3,_gpio_4,_gpio_5,_gpio_6,
_gpio_7,_gpio_8,_gpio_9,_gpio_10,_gpio_11,
_gpio_12,_gpio_13,_gpio_14,_gpio_15,_gpio_8,
_gpio_9,_gpio_10,_gpio_11,_gpio_12,_gpio_13,
_gpio_14,_gpio_15,_gpio_3,_gpio_4,_gpio_5,
_gpio_6,_gpio_7,_gpio_0,_gpio_1,_gpio_0,
_gpio_1,_gpio_0,_gpio_1,_gpio_4,_gpio_5,
_gpio_6,_gpio_7,_gpio_0,_gpio_1,_gpio_0,
_gpio_1,_gpio_2,_gpio_3,_gpio_4,_gpio_5,
};
static const uint8_t _adc_group[]=
{
0,0,_adc_a,_adc_a,0,
0,0,0,0,0,
0,0,0,0,0,
0,0,0,0,0,
0,0,0,0,0,
0,0,0,0,0,
0,0,0,0,0,
0,0,0,0,0,
0,0,0,0,0,
0,0,0,0,0,
0,0,0,0,0,
0,0,0,0,0,
0,_adc_a,_adc_a,_adc_a,_adc_a,
_adc_a,_adc_a,_adc_a,_adc_a,_adc_a,
_adc_a,_adc_a,_adc_a,_adc_a,_adc_a,
};
static const uint8_t _adc_port[]=
{
0,0,2,3,0,
0,0,0,0,0,
0,0,0,0,0,
0,0,0,0,0,
0,0,0,0,0,
0,0,0,0,0,
0,0,0,0,0,
0,0,0,0,0,
0,0,0,0,0,
0,0,0,0,0,
0,0,0,0,0,
0,0,0,0,0,
0,0,1,4,5,
6,7,8,9,10,
11,12,13,14,15,
};
static const uint16_t _tim_group[]=
{
_tim_a,_tim_a,_tim_b,_tim_b,0,
0,_tim_a,_tim_a,0,_tim_d,
_tim_d,_tim_d,_tim_d,0,0,
0,0,0,0,0,
0,0,0,0,0,
0,0,0,0,0,
0,0,0,0,0,
0,0,0,0,0,
0,0,0,0,0,
0,0,0,0,0,
0,0,0,0,0,
0,0,0,0,0,
0,_tim_b,_tim_b,0,0,
_tim_c,_tim_c,_tim_c,_tim_c,0,
0,0,0,0,0,
};
static const uint8_t _tim_port[]=
{
_tim_2,_tim_3,_tim_3,_tim_4,0,
0,_tim_1,_tim_4,0,_tim_1,
_tim_2,_tim_3,_tim_4,0,0,
0,0,0,0,0,
0,0,0,0,0,
0,0,0,0,0,
0,0,0,0,0,
0,0,0,0,0,
0,0,0,0,0,
0,0,0,0,0,
0,0,0,0,0,
0,0,0,0,0,
0,_tim_1,_tim_2,0,0,
_tim_1,_tim_2,_tim_3,_tim_4,0,
0,0,0,0,0,
};
static const uint8_t _dma1_port[]=
{
_dma_c,_dma_f,_dma_a,_dma_g,0,
0,_dma_b,0,0,_dma_a,
_dma_d,_dma_e,0,0,0,
0,0,0,0,0,
0,0,0,0,0,
0,0,0,0,0,
0,0,0,0,0,
0,0,0,0,0,
0,0,0,0,0,
0,0,0,0,0,
0,0,0,0,0,
0,0,0,0,0,
0,_dma_e,_dma_g,0,0,
_dma_f,0,_dma_b,_dma_c,0,
0,0,0,0,0,
};


static const uint8_t _usart_group[]=
{
 0x4E,0x11,0x12,0x00,0x00,
};
static const uint8_t _usart_port[]=
{
 0x10,0x32,0x54,0x00,0x00,
};
static const uint8_t _usart_rcc_basic[]=
{
 0x02,0x11,0x21,0x00,0x00,
};
#else
;
#endif
#define _gpio_data(p) ((_gpio_group[p]<<4)+_gpio_port[p])
#define _gpio_bsc(p) ((0x20+(_gpio_data(p)))>>4)
#define _gpio_rcc(p) (1<<_gpio_bsc(p))
#define _gpio_grp(p) ((GPIO_TypeDef *)((_gpio_bsc(p)<<10)+0x40010000))
#define _gpio_prt(p) (_gpio_port[p])
void setmode(uint8_t,uint8_t);

#define _adc_data(p) (_adc_group[p])
#define _adc_rcc(p) (1<<(_adc_data(p)>>2))
#define _adc_grp(p) ((ADC_TypeDef *)((uint32_t)((_adc_data(p)<<8))+0x40010000))
#define _adc_prt(p) (_adc_port[p])
void setadc(uint8_t);

#define _tim_data(p) ((_tim_group[p]<<4)+_tim_port[p])
#define isAdvancedtimer(p) ((_tim_group[p]>>8)&0x01)
#define _tim_data(p) ((_tim_group[p]<<4)+_tim_port[p])
#define _tim_rcc(p) ((isAdvancedtimer(p))?(1<<(11+((_tim_group[p]&0x10)>>3))):(1<<(_tim_group[p]>>2)))
#define _tim_grp(p) ((TIM_TypeDef *)((_tim_group[p]<<8)+0x40000000))
#define _tim_prt(p) (_tim_port[p])
#define _dma1_prt(p) ((DMA_Channel_TypeDef *)(0x40020000+_dma1_port[p]))

#define _usart_rcc_grp(p) (*(unsigned int *)((unsigned int)\
(0x40021018+0x04*(_usart_rcc_basic[p]&0x01))))
#define _usart_rcc(p) ((_usart_rcc_basic[p]&0xFE)<<13)
#define _usart_grp(p) ((USART_TypeDef *)(0x40000000+(_usart_group[p]<<10)))
#define _usart_prt_tx(p) (_usart_port[p]&0x0F)
#define _usart_prt_rx(p) (_usart_port[p]>>4)
void settim(uint8_t);
void freetim(uint8_t pin);
void pinMode(uint8_t,uint8_t);
#define digitalWrite(pin,state)	(_gpio_grp(pin)->BSRR|=((uint32_t)1<<(_gpio_prt(pin)+(16*(!state)))))
#define digitalRead(pin) ((_gpio_grp(pin)->IDR>>_gpio_prt(pin))&0x01)
uint16_t analogRead(uint8_t);

#ifdef __cplusplus
}
#endif

#endif

/***************************************************************
*Copyright(c) 2020思特诺(Stduino)All right reserved.
*
*This library is open source and free for individual users. 
*
*For commercial use, please contact server001@stduino.com.
***************************************************************/
